<?php

require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal ('Shaun');
echo "Name : " .$sheep->name. "<br>";
echo "Legs : " .$sheep->legs. "<br>";
echo "Cold Blooded : " .$sheep->cold_blooded. "<br><br>";

$frog = new Frog ('Buduk');
echo "Name : " .$frog->name. "<br>";
echo "Legs : " .$frog->legs. "<br>";
echo "Cold Blooded : " .$frog->cold_blooded. "<br>";
echo $frog->jump();

$ape = new Ape ('Kera Sakti');
echo "<br><br>Name : " .$ape->name. "<br>";
echo "Legs : " .$ape->legs. "<br>";
echo "Cold Blooded : " .$ape->cold_blooded. "<br>";
echo $ape->yell();

?>